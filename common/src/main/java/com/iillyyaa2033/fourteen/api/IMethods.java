package com.iillyyaa2033.fourteen.api;

public interface IMethods {

  /**
   * Запрашиваем посты в блоге, отсортированные по дате
   * 
   * @param blog Имя блога
   * @param count Количество постов
   * @param offset Смещение от начала
   * @param asc true = по возрастанию
   * @return список постов
   */
  Post[] listPosts(String blog, long count, long offset, boolean asc);
}
