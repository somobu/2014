package com.iillyyaa2033.fourteen.api;

/**
 */
public class Post {

  /**
   * Имя блога?
   */
  public String blog;

  /**
   * Может ли текущий пользователь изменять пост
   */
  public boolean editable;

  /**
   * Идентификатор поста
   */
  public long id;

  /**
   * Идентификатор источника
   */
  public long source;

  /**
   * Заголовок поста
   */
  public String title;

  /**
   * Текст поста, отформатирован в html или md
   */
  public String post;

  /**
   * Время публикации
   */
  public long time;

  /**
   * Айдишник автора
   */
  public long author_id;

  /**
   * Имя автора, отображаемое на посте (TODO: убрать)
   */
  public String author_pubname;

  /**
   * Имя файла изображения заголовка
   */
  public String header_image;

}
