package com.iillyyaa2033.fourteen.routing;

import com.iillyyaa2033.fourteen.Database;
import com.iillyyaa2033.fourteen.Platform;
import com.iillyyaa2033.fourteen.Utils;
import com.iillyyaa2033.fourteen.model.Methods;

import java.net.URI;
import java.sql.SQLException;
import java.util.Map;

public class Router {

    public static Page route(URI uri) {

        String blog = uri.getAuthority();
        String method = uri.getPath();
        String fragment = uri.getFragment();

        method = method.substring(1);

        switch (method) {
            case Methods.VIEW_BLOG:
                return mainPage(blog, Utils.parseLong(fragment, 1));
            case Methods.VIEW_POST:
                return singlePost(blog, Utils.parseLong(fragment, 1));
            case Methods.VIEW_DATE:
                Map<String, String> query = Utils.splitQuery(uri);
                long since = Utils.parseLong(query.get("since"), 1);
                long d_page = Utils.parseLong(fragment, 1);
                return byDate(blog, query.get("mode"), since, d_page);
            case Methods.VIEW_CATEGORY:
                long cat = Utils.parseLong(Utils.splitQuery(uri).get("cat"), 1);
                long c_page = Utils.parseLong(fragment, 1);
                return byCategory(blog, cat, c_page);
        }

        return null;
    }

    private static void basicPost(Page result, Database db) throws SQLException {
        result.editable = db.editable;
        result.blog = db.blog;
        result.blog_name = db.getSetting("blog_name");
    }

    private static PostsListPage mainPage(String blog, long page) {
        PostsListPage result = new PostsListPage();

        try {
            Database db = Platform.database(blog);
            basicPost(result, db);

            result.page = page;
            result.total = 10;
            result.posts = db.queryPosts(page);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

    private static PostsListPage byDate(String blog, String mode, long since, long page) {
        PostsListPage result = new PostsListPage();

        try {
            Database db = Platform.database(blog);
            basicPost(result, db);

            result.page = page;
            result.total = 10;
            result.datefilter = since;

            switch (mode) {
                case "day":
                    result.posts = db.queryPostsByTimeSince(since, 1000 * 60 * 60 * 24, 1);
                    result.message = PostsListPage.Message.FILTER_BY_DAY;
                    break;
                case "month":
                    result.posts = db.queryPostsByTimeSince(since, 1000 * 60 * 60 * 24 * 31L, 1);
                    result.message = PostsListPage.Message.FILTER_BY_MONTH;
                    break;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

    private static PostsListPage byCategory(String blog, long cat, long page) {
        PostsListPage result = new PostsListPage();

        try {
            Database db = Platform.database(blog);
            basicPost(result, db);

            result.page = page;
            result.total = 10;
            result.posts = db.queryPostsByCategory(cat, page);
            result.message = PostsListPage.Message.FILTER_BY_CATEGORY;
            result.catname = db.getCatName(cat);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

    private static SinglePostPage singlePost(String blog, long postId) {
        SinglePostPage result = new SinglePostPage();

        try {
            Database db = Platform.database(blog);
            basicPost(result, db);

            result.post = db.getPostById(postId);
            if(result.post != null) {
                result.prev = db.getPostBefore(result.post.time);
                result.next = db.getPostAfter(result.post.time);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }
}
