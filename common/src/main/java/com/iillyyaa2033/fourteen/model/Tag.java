package com.iillyyaa2033.fourteen.model;

public class Tag {

    public long id;
    public String codename;
    public String nicename;

    public Tag(String codename, String nicename){
        this.codename = codename;
        this.nicename = nicename;
    }

    public Tag(long id, String codename, String nicename){
        this.id = id;
        this.codename = codename;
        this.nicename = nicename;
    }

    @Override
    public String toString() {
        return codename+": "+nicename;
    }
}
