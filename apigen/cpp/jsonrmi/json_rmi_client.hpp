#include <sys/socket.h>
#include <sys/un.h>
#include <string>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <utility>
#include <json.h>

#pragma once

struct JsonRpcRequest;

class JsonRpcClient {
private:
    struct sockaddr_un addr{};
    int fd;

    std::thread* reader_thread;

    // Все переменные в этом блоке должны использоваться строго из-под мьютекса
    std::mutex fcn_call_mtx; // Да, из-под этого мьютекса
    int methodIdCounter = 0;
    std::vector<JsonRpcRequest *> requests;
    // Окончание этого блока -- дальше мьютекс не нужен

    Json::Reader reader;
    Json::StreamWriterBuilder builder;

public:

    /**
     * Конструктор, да
     * @param socket путь к сокету сервера
     */
    explicit JsonRpcClient(const std::string &socket_path);

    /**
     * Запускаем поток клиента и коннектимся к серверу
     */
    void start();

    /**
     * Дисконнектимся и останавливаем поток клиента
     */
    void stop();

    /**
     * Подсовываем объект, на котором будем вызывать запросы сервера
     *
     * @param callback
     */
    void setCallback(void *callback);

    /**
     * Блокирующий вызов метода на сервере
     *
     * @param method
     * @param args
     */
    Json::Value call(const std::string &method, Json::Value &args);
};