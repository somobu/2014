package com.kitsuki.jsonrpc;

class Request {

    long rqCode;
    String methodName;
    Object[] args;

    Request(long rqCode, String methodName, Object[] args) {
        this.rqCode = rqCode;
        this.methodName = methodName;
        this.args = args;
    }

}
