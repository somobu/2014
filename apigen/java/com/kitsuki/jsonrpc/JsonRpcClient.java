package com.kitsuki.jsonrpc;

import org.newsclub.net.unix.AFUNIXSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;

import java.io.File;
import java.io.IOException;

public class JsonRpcClient<M> extends UnixSocketPeer {

    /**
     * We'll call this on remote server
     */
    private final M remotes;

    public JsonRpcClient(File socketFile, Class<M> targetClass, Object callback) throws IOException {
        if (targetClass == null) throw new RuntimeException("Target class cannot be null");

        AFUNIXSocket sock = AFUNIXSocket.newInstance();
        sock.connect(new AFUNIXSocketAddress(socketFile));

        start(false, sock, callback);
        remotes = (M) getRemoteInterface(false, targetClass);
    }

    public void stopClient() throws IOException {
        close();
    }

    public M getRemoteMethods() {
        return (M) remotes;
    }


}
