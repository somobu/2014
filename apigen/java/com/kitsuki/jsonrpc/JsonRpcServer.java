package com.kitsuki.jsonrpc;

import org.newsclub.net.unix.AFUNIXServerSocket;
import org.newsclub.net.unix.AFUNIXSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class JsonRpcServer<Methods, Callback> extends Thread {

    private final Logger log = LoggerFactory.getLogger(JsonRpcServer.class);

    private AFUNIXServerSocket server = null;
    private final File socketFile;
    private final Class<Callback> callback;
    private final List<UnixSocketPeer> peers = Collections.synchronizedList(new ArrayList<>());

    public JsonRpcServer(File socket) {
        this(socket, null);
    }

    public JsonRpcServer(File socket, Class<Callback> callback) {
        this.socketFile = socket;
        this.callback = callback;
    }

    public void startServer() throws IOException {
        server = AFUNIXServerSocket.newInstance();
        server.bind(new AFUNIXSocketAddress(socketFile));
        start();
    }

    public void stopServer() {
        interrupt();
    }

    public abstract Methods getMethods(Callback callback);

    public List<Callback> all() {
        ArrayList<Callback> list = new ArrayList<>();

        synchronized (peers) {
            for (UnixSocketPeer peer : peers) {
                list.add((Callback) peer.getRemoteInterface());
            }
        }

        return list;
    }

    @Override
    public void run() {

        try {
            while (!isInterrupted()) {
                AFUNIXSocket sock = server.accept();
                UnixSocketPeer client = new UnixSocketPeer();
                client.setL(peer -> peers.remove(peer));
                synchronized (peers) {
                    peers.add(client);
                }

                Methods m = getMethods((Callback) client.getRemoteInterface(true, callback));
                client.start(true, sock, m);
            }

        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (server != null && !server.isClosed()) {
                log.debug("Stopping server");

                try {
                    server.close();
                } catch (IOException e) {
                    log.error("Unable to stop server", e);
                }

                try {
                    socketFile.delete();
                } catch (Exception e) {
                    log.error("Unable to delete socket file", e);
                }
            }
        }
    }
}
