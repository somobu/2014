#include "model.hpp"


Json::Value Post::to_json(){
  Json::Value json;
  json["blog"] = blog;
  json["editable"] = editable;
  json["id"] = id;
  json["source"] = source;
  json["title"] = title;
  json["post"] = post;
  json["time"] = time;
  json["author_id"] = author_id;
  json["author_pubname"] = author_pubname;
  json["header_image"] = header_image;
  return json;
}


std::shared_ptr<Post> Post::from_json(Json::Value& json){
  Post *result = new Post();
  result->blog = json["blog"].asString();
  result->editable = json["editable"].asBool();
  result->id = json["id"].asInt64();
  result->source = json["source"].asInt64();
  result->title = json["title"].asString();
  result->post = json["post"].asString();
  result->time = json["time"].asInt64();
  result->author_id = json["author_id"].asInt64();
  result->author_pubname = json["author_pubname"].asString();
  result->header_image = json["header_image"].asString();
  std::shared_ptr<Post> ptr(result);
  return ptr;
}

