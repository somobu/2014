package com.iillyyaa2033.fourteen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.iillyyaa2033.fourteen.model.Source;

import java.util.ArrayList;
import java.util.Arrays;

public class SourcesActivity extends CustomizedActivity {

    private ArrayAdapter<Source> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FourteenDB db = FourteenDB.open(this, Const.LOCAL_BLOG);
        Source[] list = db.getAllSources();
        db.close();

        ListView listView = new ListView(this);
        setContentView(listView);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<>(Arrays.asList(list)));
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                new AlertDialog.Builder(SourcesActivity.this)
                        .setItems(new String[]{getString(R.string.rename), getString(R.string.merge), getString(R.string.remove)}, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                switch(i){
                                    case 0:
                                        showRename(adapter.getItem(position));
                                        break;
                                    case 1:
                                        Toast.makeText(SourcesActivity.this, "Unimplemented yet", Toast.LENGTH_LONG).show();
                                        break;
                                    case 2:
                                        showDelete(adapter.getItem(position));
                                        break;
                                }
                            }
                        })
                        .show();
            }
        });
    }

    void showRename(final Source item){
        final EditText text = new EditText(this);
        text.setText(item.name);

        new AlertDialog.Builder(this)
                .setView(text)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FourteenDB db = FourteenDB.open(SourcesActivity.this, Const.LOCAL_BLOG);
                        db.updateSource(new Source(item.id, text.getText().toString()));
                        db.close();

                        item.name = text.getText().toString();
                    }
                })
                .show();
    }

    void showDelete(final Source item){
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.sources_confirm_deletion, item.name))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FourteenDB db = FourteenDB.open(SourcesActivity.this, Const.LOCAL_BLOG);
                        db.deletePostsFromSource(item.id);
                        db.deleteSource(item.id);

                        adapter.clear();
                        adapter.addAll(db.getAllSources());
                        adapter.notifyDataSetChanged();

                        db.close();
                    }
                })
                .show();

    }
}
