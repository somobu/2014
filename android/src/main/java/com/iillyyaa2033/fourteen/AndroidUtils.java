package com.iillyyaa2033.fourteen;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;

public class AndroidUtils {

    public static void setClickableText(TextView view, String html, final LinkClick listener){
        CharSequence sq = Html.fromHtml(html);
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder(sq);

        URLSpan[] spans = stringBuilder.getSpans(0, sq.length(), URLSpan.class);
        for(final URLSpan span : spans){

            ClickableSpan cs = new ClickableSpan() {

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }

                @Override
                public void onClick(View widget) {
                    listener.onLinkClick(span.getURL());
                }
            };
            stringBuilder.setSpan(cs, stringBuilder.getSpanStart(span), stringBuilder.getSpanEnd(span), stringBuilder.getSpanFlags(span));
            stringBuilder.removeSpan(span);
        }

        view.setText(stringBuilder);
        view.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public interface LinkClick {
        void onLinkClick(String url);
    }

}
