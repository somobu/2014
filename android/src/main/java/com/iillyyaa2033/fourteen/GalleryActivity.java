package com.iillyyaa2033.fourteen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class GalleryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        fillSpinner();
    }

    void fillSpinner() {
        File root = new File(Const.EXT_ROOT, Const.LOCAL_BLOG);

        final ArrayList<String> items = new ArrayList<>();

        if (root.exists()) {
            for (File year : root.listFiles()) {
                if (year.isDirectory()) {
                    for (File month : year.listFiles()) {
                        if (month.isDirectory()) {
                            items.add(year.getName() + "/" + month.getName());
                        }
                    }
                }
            }
        }
        Collections.sort(items);

        Spinner sp = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fillList(Const.LOCAL_BLOG, "/" + items.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp.setSelection(0);
    }

    void fillList(final String blog, final String path) {
        final ArrayList<String> filenames = new ArrayList<>();
        File file = new File(Const.EXT_ROOT, blog + "/" + path);

        if (file.exists()) {
            for (File day : file.listFiles()) {
                if (day.isDirectory()) {
                    for (File item : day.listFiles()) {
                        if (item.isFile()) {
                            filenames.add(path + "/" + day.getName() + "/" + item.getName());
                        }
                    }
                }
            }
        }

        GridView grid = findViewById(R.id.grid);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, filenames) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ImageView image = new ImageView(getContext());

                image.setAdjustViewBounds(true);
                image.setPadding(24, 24, 24, 24);

                Picasso.get().load(new File(Const.EXT_ROOT, blog + "/" + getItem(position))).memoryPolicy(MemoryPolicy.NO_CACHE).resize(512, 512).centerCrop().into(image);

                return image;
            }
        };
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.putExtra("image", filenames.get(i));
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
