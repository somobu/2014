package com.iillyyaa2033.fourteen;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.net.URI;

public class BlogSettingsActivity extends CustomizedActivity {

    private final int REQUEST_PICK = 1;
    File output = null;
    String blog;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogsettings);
        
        // Process link
        Uri uri = getIntent().getData();
        if(uri != null){
            blog = URI.create(uri.toString()).getAuthority();
        } else {
            blog = Const.LOCAL_BLOG;
        }
        
        output = new File(Const.EXT_ROOT, "/"+blog+"/header_image.jpg");

        final EditText username = findViewById(R.id.username);
        final EditText blogname = findViewById(R.id.blogname);

        FourteenDB db = FourteenDB.open(this, blog);
        username.setText(db.getPublicAuthorName(1));
        blogname.setText(db.getSetting(FourteenDB.Setting.BLOG_NAME));
        db.close();

        Button button = findViewById(R.id.button);
        boolean b = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("STARTUP_DONE", false);
        button.setText(b ? R.string.startup_updateblog : R.string.startup_createblog);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameStr = username.getText().toString();
                String blognameStr = blogname.getText().toString();

                if(usernameStr.isEmpty()) usernameStr = getString(R.string.default_user_name);

                FourteenDB db = FourteenDB.open(BlogSettingsActivity.this, blog);
                db.setAuthor("admin", usernameStr);
                db.updateSetting(FourteenDB.Setting.BLOG_NAME, blognameStr);
                db.close();

                finishStartup();
            }
        });

        ImageView headerPreview = findViewById(R.id.headerPreview);
        TextView delHint = findViewById(R.id.image_del2);
        headerPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pick();
            }
        });
        delHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                del();
            }
        });

        if(output.exists()){
            headerPreview.setImageURI(Uri.fromFile(output));
            delHint.setVisibility(View.VISIBLE);
        } else {
            delHint.setVisibility(View.GONE);
        }

        if(!b)
            findViewById(R.id.optional_root).setVisibility(View.GONE);

        findViewById(R.id.manage_sources).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BlogSettingsActivity.this, SourcesActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case REQUEST_PICK:
                    // Start cropping:
                    UCrop.of(data.getData(), Uri.fromFile(output))
                            .withAspectRatio(21, 4)
                            .start(this);
                    break;
                case UCrop.REQUEST_CROP:
                    if(data != null){
                        ((ImageView) findViewById(R.id.headerPreview)).setImageURI(UCrop.getOutput(data));
                        findViewById(R.id.image_del2).setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(this, "No image provided", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }

    void pick(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "TODO: translated title"), REQUEST_PICK);
    }

    void del(){
        findViewById(R.id.image_del2).setVisibility(View.GONE);
        output.delete();
        ((ImageView) findViewById(R.id.headerPreview)).setImageResource(android.R.drawable.ic_menu_report_image);
    }

    void finishStartup(){
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("STARTUP_DONE",true).apply();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
